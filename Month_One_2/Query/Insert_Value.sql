--выполнить 1 раз для создания UUID
--CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
------Добавляем банки
INSERT INTO "Banks" ("Id", "Town", "Name", "Raiting", etl_date)
  VALUES 
  ---
  (uuid_generate_v1(), 'Москва', 'Первое отделение', 4, NOW()),
  (uuid_generate_v1(), 'Москва', 'Второе отделение', 5, NOW()),
  (uuid_generate_v1(), 'Москва', 'Третье отделение', 3, NOW()),
  (uuid_generate_v1(), 'Санкт-Питербург', 'Первый отдел', 2, NOW()),
  (uuid_generate_v1(), 'Санкт-Питербург', 'Второй отдел', 4, NOW()),
  (uuid_generate_v1(), 'Санкт-Питербург', 'Третий отдел', 5, NOW());
------Добавляем клиентов
INSERT INTO "Clinets" ("Id", "SurName", "FirstName", "LastName", "Sex", "DateOfBirth", "BankId", etl_date)
  VALUES 
  ---
  (uuid_generate_v1(), E'Попов', E'Егор', E'Михайлович', E'М', NOW(), (SELECT "Id" FROM "Banks" WHERE "Town" = 'Москва' AND "Name" = 'Первое отделение' LIMIT 1), NOW()),
  (uuid_generate_v1(), E'Маслова', E'Яна', E'Данииловна', E'Ж', NOW(), (SELECT "Id" FROM "Banks" WHERE "Town" = 'Москва' AND "Name" = 'Первое отделение' LIMIT 1), NOW()),

  (uuid_generate_v1(), E'Дорофеева', E'Виктория', E'Леонидовна', E'Ж', NOW(), (SELECT "Id" FROM "Banks" WHERE "Town" = 'Москва' AND "Name" = 'Второе отделение' LIMIT 1), NOW()),
  (uuid_generate_v1(), E'Панин', E'Матвей', E'Максимович', E'М', NOW(), (SELECT "Id" FROM "Banks" WHERE "Town" = 'Москва' AND "Name" = 'Второе отделение' LIMIT 1), NOW()),

  (uuid_generate_v1(), E'Королева', E'Варвара', E'Данииловна', E'Ж', NOW(), (SELECT "Id" FROM "Banks" WHERE "Town" = 'Москва' AND "Name" = 'Третье отделение' LIMIT 1), NOW()),
  (uuid_generate_v1(), E'Иванова', E'Ева', E'Сергеевна', E'Ж', NOW(), (SELECT "Id" FROM "Banks" WHERE "Town" = 'Москва' AND "Name" = 'Третье отделение' LIMIT 1), NOW()),

  (uuid_generate_v1(), E'Кудряшова', E'Анна', E'Владиславовна', E'Ж', NOW(), (SELECT "Id" FROM "Banks" WHERE "Town" = 'Санкт-Питербург' AND "Name" = 'Третий отдел' LIMIT 1), NOW()),
  (uuid_generate_v1(), E'Давыдова', E'Елизавета', E'Артёмовна', E'Ж', NOW(), (SELECT "Id" FROM "Banks" WHERE "Town" = 'Санкт-Питербург' AND "Name" = 'Третий отдел' LIMIT 1), NOW()),

  (uuid_generate_v1(), E'Новикова', E'Василиса', E'Александровна', E'Ж', NOW(), (SELECT "Id" FROM "Banks" WHERE "Town" = 'Санкт-Питербург' AND "Name" = 'Второй отдел' LIMIT 1), NOW()),
  (uuid_generate_v1(), E'Гаврилова', E'Алина', E'Семёновна', E'Ж', NOW(), (SELECT "Id" FROM "Banks" WHERE "Town" = 'Санкт-Питербург' AND "Name" = 'Второй отдел' LIMIT 1), NOW()),

  (uuid_generate_v1(), E'Мельникова', E'Марина', E'Германовна', E'Ж', NOW(), (SELECT "Id" FROM "Banks" WHERE "Town" = 'Санкт-Питербург' AND "Name" = 'Первый отдел' LIMIT 1), NOW()),
  (uuid_generate_v1(), E'Лукьянов', E'Егор', E'Георгиевич', E'М', NOW(), (SELECT "Id" FROM "Banks" WHERE "Town" = 'Санкт-Питербург' AND "Name" = 'Первый отдел' LIMIT 1), NOW());
------Добавляем транзакции
------Тут надо указывать клиентов
INSERT INTO "Transactions"
---
("Id", "ClientId", "Value", "IsCancelling", etl_date) 
VALUES
---
(uuid_generate_v1(), (SELECT "Id" FROM "Clinets" LIMIT 1), (SELECT random() * 10000), false, NOW()),
(uuid_generate_v1(), (SELECT "Id" FROM "Clinets" LIMIT 1), (SELECT random() * 10000), false, NOW()),
(uuid_generate_v1(), (SELECT "Id" FROM "Clinets" LIMIT 1), (SELECT random() * 10000), false, NOW()),
(uuid_generate_v1(), (SELECT "Id" FROM "Clinets" LIMIT 1), (SELECT random() * 10000), false, NOW()),
(uuid_generate_v1(), (SELECT "Id" FROM "Clinets" LIMIT 1), (SELECT random() * 10000), false, NOW());
