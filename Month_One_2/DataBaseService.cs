﻿using Microsoft.EntityFrameworkCore;
using Month_One_2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Month_One_2
{
    public class DataBaseService
    {
        private DataBaseContext _context { get; set; }
        private Action<string> _log { get; set; }
        public DataBaseService(DataBaseContext context, Action<string> log)
        {
            _context = context;
            _log = log;
        }
        /// <summary>
        /// Общая информация о БД
        /// </summary>
        public async Task WriteInfo()
        {
            _log($"Имя БД Month_One_2");
            _log($"Таблица Banks - {_context.Banks.Count()} write (Содержит банки)");
            _log($"Таблица Clinet - {_context.Clinets.Count()} write (Содержит клиентов)");
            _log($"Таблица Transactions - {_context.Transactions.Count()} write (Содержит транзакции клиентов)");
        }
        /// <summary>
        /// Очистка БД
        /// </summary>
        public async Task<bool> CleadDB()
        {
            try
            {
                if (_context == null)
                {
                    _log("Ошибка подключения.");
                    return false;
                }
                //Надо начать удалять с меньшей сущности из за внешних ключей
                if (await _context.Transactions.AnyAsync())
                {
                    var _removeTransactions = await _context.Transactions.ToListAsync();
                    foreach (var _transaction in _removeTransactions)
                        _context.Transactions.Remove(_transaction);

                    await _context.SaveChangesAsync();
                    _log("Транзакции очищены.");
                }
                if (await _context.Clinets.AnyAsync())
                {
                    var _removeClients = await _context.Clinets.ToListAsync();
                    foreach (var _client in _removeClients)
                        _context.Clinets.Remove(_client);

                    await _context.SaveChangesAsync();
                    _log("Клиенты очищены.");
                }
                if (await _context.Banks.AnyAsync())
                {
                    var _removeBanks = await _context.Banks.ToListAsync();
                    foreach (var _bank in _removeBanks)
                        _context.Banks.Remove(_bank);

                    await _context.SaveChangesAsync();
                    _log("Банки очищены.");
                }
                return true;
            }
            catch
            (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Загрузка БД данными если их нет
        /// </summary>
        public async Task<bool> LoadDB()
        {
            try
            {
                Random _rnd = new Random();

                if (_context == null)
                {
                    _log("Ошибка подключения.");
                    return false;
                }
                if (!await _context.Banks.AnyAsync())
                {
                    _log("Банки отсутствуют. Заполняем.");
                    await _context.Banks.AddAsync(new Bank() { Raiting = _rnd.Next(1, 5), Town = "Москва", Name = "Первое отделение" });
                    await _context.Banks.AddAsync(new Bank() { Raiting = _rnd.Next(1, 5), Town = "Москва", Name = "Второе отделение" });
                    await _context.Banks.AddAsync(new Bank() { Raiting = _rnd.Next(1, 5), Town = "Москва", Name = "Третье отделение" });
                    await _context.Banks.AddAsync(new Bank() { Raiting = _rnd.Next(1, 5), Town = "Санкт-Питербург", Name = "Первый отдел" });
                    await _context.Banks.AddAsync(new Bank() { Raiting = _rnd.Next(1, 5), Town = "Санкт-Питербург", Name = "Второй отдел" });
                    await _context.Banks.AddAsync(new Bank() { Raiting = _rnd.Next(1, 5), Town = "Санкт-Питербург", Name = "Третий отдел" });
                    await _context.SaveChangesAsync();
                    _log($"\tДобавлено банков: 6");
                }
                if (!await _context.Clinets.AnyAsync())
                {
                    _log("Клиенты отсутствуют. Заполняем.");
                    var _list_name = GetName();
                    var _banks = await _context.Banks.ToListAsync();
                    foreach (var _bank in _banks)
                    {
                        int _countClient = _rnd.Next(3, 7);
                        for (int i = 1; i <= _countClient; i++)
                        {
                            var _name = _list_name[_rnd.Next(_list_name.Count)];
                            await _context.Clinets.AddAsync(new Client() { BankId = _bank.Id, SurName = _name.Name1, FirstName = _name.Name2, LastName = _name.Name3, Sex = _name.Sex, DateOfBirth = _name.DateOfBirth() });
                        }
                        _log($"\tДля банка: {_bank.Town}/{_bank.Name} добавлено клиентов: {_countClient}");
                    }
                    await _context.SaveChangesAsync();
                }
                if (!await _context.Transactions.AnyAsync())
                {
                    _log("Транзакции отсутствуют. Заполняем.");
                    var _clients = await _context.Clinets.ToListAsync();
                    foreach (var _client in _clients)
                    {
                        int _countTransactions = _rnd.Next(50, 100);
                        for (int i = 1; i <= _countTransactions; i++)
                        {
                            await _context.Transactions.AddAsync(new Transaction() { ClientId = _client.Id, Value = GetRandomDouble(1, 20000), IsCancelling = GetIsCancelling() });
                        }
                        _log($"\tДля клиента: {_client.SurName} {_client.FirstName} добавлено транзакций: {_countTransactions}");
                    }
                    await _context.SaveChangesAsync();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Получить полный список банков
        /// </summary>
        public async Task<List<Bank>> GetBanks() => await _context.Banks.ToListAsync();
        /// <summary>
        /// Получить 1 банк по ID
        /// </summary>
        /// <param name="Id">ID Банка</param>
        public async Task<Bank> GetBankId(Guid Id) => await _context.Banks.FindAsync(Id);
        /// <summary>
        /// Получить полный список клиентов
        /// </summary>
        public async Task<List<Client>> GetClients() => await _context.Clinets.ToListAsync();
        /// <summary>
        /// Получить клиенты определённого банка
        /// </summary>
        /// <param name="BankId">ID Банка</param>
        public async Task<List<Client>> GetClientsBank(Guid BankId) => await _context.Clinets.Where(s => s.BankId == BankId).ToListAsync();
        /// <summary>
        /// Получить 1 клиента по ID
        /// </summary>
        /// <param name="Id">ID Клиента</param>
        public async Task<Client> GetClientId(Guid Id) => await _context.Clinets.FindAsync(Id);
        /// <summary>
        /// Получить полный список транзакций
        /// </summary>
        public async Task<List<Transaction>> GetTransactions() => await _context.Transactions.ToListAsync();
        /// <summary>
        /// Получить список транзакций клиента
        /// </summary>
        /// <param name="ClientId">ID Клиента</param>
        public async Task<List<Transaction>> GetTransactionsClient(Guid ClientId) => await _context.Transactions.Where(s => s.ClientId == ClientId).ToListAsync();
        /// <summary>
        /// Получить список транзакции банка
        /// </summary>
        /// <param name="BankId">ID Банка</param>
        /// <returns></returns>
        public async Task<List<Transaction>> GetTransactionsBank(Guid BankId)
        {
            var _clients = await GetClientsBank(BankId);
            return await _context.Transactions.Where(s => _clients.Select(x => x.Id).Contains(s.ClientId)).ToListAsync();
        }
        /// <summary>
        /// Получить 1 транзакцию по ID
        /// </summary>
        /// <param name="Id">ID Транзакции</param>
        public async Task<Transaction> GetTransactionId(Guid Id) => await _context.Transactions.FindAsync(Id);
        public async Task<bool> CreateBank(Bank model)
        {
            if (model == null) return false;
                await _context.Banks.AddAsync(model);
                await _context.SaveChangesAsync();
            return true;
        }

        #region Secondary
        private List<RassianName> GetName()
        {
            List<RassianName> _returnRussianNames = new List<RassianName>();
            //Мужчины
            using (var reader = new StreamReader(@"Query\FIO_man.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine().Split(';');
                    _returnRussianNames.Add(new RassianName()
                    {
                        Name1 = line[0],
                        Name2 = line[1],
                        Name3 = line[2],
                        Sex = 'М'
                    });
                }
            }
            //Женщины
            using (var reader = new StreamReader(@"Query\FIO_woman.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine().Split(';');
                    _returnRussianNames.Add(new RassianName()
                    {
                        Name1 = line[0],
                        Name2 = line[1],
                        Name3 = line[2],
                        Sex = 'Ж'
                    });
                }
            }
            return _returnRussianNames;
        }
        /// <summary>
        /// Список русский ФИО (для заполнения клиентов)
        /// </summary>
        protected class RassianName
        {
            public string Name1 { get; set; }
            public string Name2 { get; set; }
            public string Name3 { get; set; }
            public char Sex { get; set; }
            /// <summary>
            /// Получение случайно даты рождения
            /// </summary>
            public DateTime DateOfBirth()
            {
                Random _rnd = new Random();
                return DateTime.Now.AddYears(-_rnd.Next(20, 70)).AddHours(_rnd.Next(1, 23)).AddDays(_rnd.Next(1, 365));
            }
        }
        /// <summary>
        /// Случайно число формата Double
        /// </summary>
        protected double GetRandomDouble(double min, double max)
        {
            Random _rndReturn = new Random();
            return _rndReturn.NextDouble() * (max - min) + min;
        }
        /// <summary>
        /// Получение случайного bool (с вероятностью 5% true)
        /// для варианта транзакции клиента
        /// </summary>
        protected bool GetIsCancelling()
        {
            Random _rnd = new Random();
            if (_rnd.Next(1, 100) < 6) return true;
            else return false;
        }
        #endregion
    }
}
