﻿using Month_One_2;
using Month_One_2.Model;

public class Program
{
    #region FoudnationAttribyte
    private static bool _exit { get; set; } = false;
    private static Menu _menu { get; set; }
    private static DataBaseContext _dataBaseContext { get; set; }
    private static DataBaseService _dateBaseService { get; set; }
    private static List<Bank> _bankList { get; set; }
    private static List<Client> _clientList { get; set; }
    private static List<Transaction> _transactionList { get; set; }
    private static Stack<StepMenu> _stackStepMenu { get; set; }
    //Формат вывода сумм
    private static string _formatSum = "### ### ##0.##";
    #endregion
    private static async Task Main(string[] args)
    {
        Log("OTUS. Месяц 1, задание 2. Создание БД и подключение к ней.");
        //Подключаем контекст БД
        _dataBaseContext = new DataBaseContext();
        //Создаём сервис управления
        _dateBaseService = new DataBaseService(_dataBaseContext, Log);
        //Создаём очеред хлебных крошек по меню
        _stackStepMenu = new Stack<StepMenu>();
        //Наполняем меню кнопками первый раз
        await ChangeMenu(StepMenu.Default);
        //Выводим меню пока не получим команду выйти из приложения
        while (!_exit)
        {
            //выводим меню
            foreach (var button in _menu.Buttons)
                Log($"{button.Button}. {button.Info}");
            //Выводим надписю и ждём ввода
            Console.Write("Введите значение:");
            string _input = Console.ReadLine();
            Console.Clear();
            //Активируем метод меню
            var _execute = _menu.Buttons.FirstOrDefault(s => s.Button == _input);
            if (_execute != null) await _execute.Execute();
            else Log("Такой кнопки нет");
        }
    }
    /// <summary>
    /// Берём данные из БД для их использования
    /// Если данных будет очень много так делать нельзя!!!
    /// </summary>
    /// <returns></returns>
    private static async Task<bool> LoadData()
    {
        _bankList = new List<Bank>();
        _bankList = await _dateBaseService.GetBanks();
        foreach (var _bank in _bankList.Select((item, index) => (item, index)))
            _bank.item.RowMenu = _bank.index + 2;

        _clientList = new List<Client>();
        _clientList = await _dateBaseService.GetClients();
        foreach (var _client in _clientList.Select((item, index) => (item, index)))
            _client.item.RowMenu = _client.index + 2;

        _transactionList = new List<Transaction>();
        _transactionList = await _dateBaseService.GetTransactions();
        foreach (var _transaction in _transactionList.Select((item, index) => (item, index)))
            _transaction.item.RowMenu = _transaction.index + 2;

        return true;
    }
    /// <summary>
    /// Создание меню. Несколько уровней. Наверное можно и по компактнее сделать.
    /// </summary>
    private static async Task ChangeMenu(StepMenu step)
    {
        if (!_stackStepMenu.Contains(step))
            _stackStepMenu.Push(step);

        var _listMenu = new List<MenuButton>();
        switch (step)
        {
            #region Default
            case StepMenu.Default:
                _listMenu.Add(new MenuButton("1", "Информация", async () => await _dateBaseService.WriteInfo()));
                _listMenu.Add(new MenuButton("2", "Заполнить БД данными", async () => { await _dateBaseService.LoadDB(); }));
                _listMenu.Add(new MenuButton("3", "Очистить БД от данных", async () => await _dateBaseService.CleadDB()));
                _listMenu.Add(new MenuButton("-", "-----------------", async () => { }));
                _listMenu.Add(new MenuButton("4", "Просмотр данных", async () => { await ChangeMenu(StepMenu.Tables); }));
                break;
            #endregion

            #region Tables
            case StepMenu.Tables:
                await LoadData();
                _listMenu.Add(new MenuButton("1", $"Банки {_bankList.Count()}", async () => await ChangeMenu(StepMenu.Banks)));
                _listMenu.Add(new MenuButton("2", $"Клиенты {_clientList.Count()}", async () => await ChangeMenu(StepMenu.Clients)));
                _listMenu.Add(new MenuButton("3", $"Транзакции {_transactionList.Count()}", async () => await ChangeMenu(StepMenu.Transactions)));
                break;
            #endregion

            #region Banks
            case StepMenu.Banks:
                _bankList.ForEach(s => s.IsSelect = false);
                _clientList.ForEach(s => s.IsSelect = false);
                _transactionList.ForEach(s => s.IsSelect = false);
                _listMenu.Add(new MenuButton($"1", $"Добавить банк", async () => { await CreateBank(); }));
                foreach (var _bank in _bankList)
                {
                    _listMenu.Add(new MenuButton($"{_bank.RowMenu}", $"{_bank.Town}/{_bank.Name} ({_bank.Raiting}/5)", async () => { _bank.IsSelect = true; await ChangeMenu(StepMenu.Bank); }));
                    _listMenu.Add(new MenuButton($"{_bank.RowMenu}-1", $"Клиенты", async () => { _bank.IsSelect = true; await ChangeMenu(StepMenu.BankByClients); }));
                    _listMenu.Add(new MenuButton($"{_bank.RowMenu}-2", $"Транзакции", async () => { _bank.IsSelect = true; await ChangeMenu(StepMenu.BanksByTransactions); }));
                }
                break;
            case StepMenu.Bank:
                var _bankMenu = _bankList.SingleOrDefault(s => s.IsSelect == true);
                _listMenu.Add(new MenuButton($"--", $"Город: {_bankMenu.Town}", async () => { }));
                _listMenu.Add(new MenuButton($"--", $"Филиал: {_bankMenu.Name}", async () => { }));
                _listMenu.Add(new MenuButton($"--", $"Рейтинг: {_bankMenu.Raiting}/5", async () => { }));
                _listMenu.Add(new MenuButton($"1", $"Клиенты {_bankMenu.ClientsCount}", async () => { await ChangeMenu(StepMenu.BankByClients); }));
                _listMenu.Add(new MenuButton($"2", $"Транзакции {_bankMenu.TransactionCount} на сумму {_bankMenu.TransactionSum.ToString(_formatSum)}", async () => { await ChangeMenu(StepMenu.BanksByTransactions); }));
                break;
            case StepMenu.BankByClients:
            case StepMenu.BanksByTransactions:
                _clientList.ForEach(s => s.IsSelect = false);
                _transactionList.ForEach(s => s.IsSelect = false);
                var _bankInfo = _bankList.SingleOrDefault(s => s.IsSelect == true);
                _listMenu.Add(new MenuButton($"--", $"{_bankInfo.Town}/{_bankInfo.Name} ({_bankInfo.Raiting}/5)", async () => { }));
                foreach (var _client in _clientList.Where(s => s.BankId == _bankInfo.Id))
                {
                    if (step == StepMenu.BankByClients)
                    {
                        _listMenu.Add(new MenuButton(
                            $"{_client.RowMenu}",
                            $"{_client.SurName} {_client.FirstName} {_client.LastName} (Сумма {_client.TransactionSum.ToString(_formatSum)})",
                            async () => { _client.IsSelect = true; await ChangeMenu(StepMenu.Client); }));
                    }
                    if (step == StepMenu.BanksByTransactions)
                    {
                        Console.Clear();
                        foreach (var _transaction in _transactionList.Where(s => s.ClientId == _client.Id))
                        {
                            _listMenu.Add(new MenuButton($"--",
                                $"Дата: {_transaction.etl_date.ToString("dd.MM.yy HH:mm")} Отмена: {(_transaction.IsCancelling ? "Да " : "Нет")} Сумма: {_transaction.Value}",
                                async () => { }));
                        }
                    }
                }
                break;
            #endregion

            #region Clients
            case StepMenu.Clients:
                _clientList.ForEach(s => s.IsSelect = false);
                _listMenu.Add(new MenuButton($"1", $"Добавить клиента (!не работает!)", async () => { }));
                foreach (var _client in _clientList)
                {
                    _listMenu.Add(new MenuButton($"{_client.RowMenu}", $"{_client.SurName} {_client.FirstName} {_client.LastName} (Сумма {_client.TransactionSum.ToString(_formatSum)})",
                        async () => { _client.IsSelect = true; await ChangeMenu(StepMenu.Client); }));
                }
                break;
            case StepMenu.Client:
                var _clientInfo = _clientList.SingleOrDefault(s => s.IsSelect == true);
                _listMenu.Add(new MenuButton($"--", $"Фамилия: {_clientInfo.SurName}", async () => { }));
                _listMenu.Add(new MenuButton($"--", $"Имя: {_clientInfo.FirstName}", async () => { }));
                _listMenu.Add(new MenuButton($"--", $"Отчество: {_clientInfo.LastName}", async () => { }));
                _listMenu.Add(new MenuButton($"--", $"Пол: {(_clientInfo.Sex == 'М' ? "Муж" : "Жен")}", async () => { }));
                _listMenu.Add(new MenuButton($"--", $"Возраст: {_clientInfo.Age.ToString("###0")}", async () => { }));
                _listMenu.Add(new MenuButton($"--", $"Сумма: {_clientInfo.TransactionSum.ToString(_formatSum)}", async () => { }));
                _listMenu.Add(new MenuButton($"1", $"Транзакции", async () => { await ChangeMenu(StepMenu.ClientByTransactions); }));
                break;
            case StepMenu.ClientByTransactions:
                Console.Clear();
                foreach (var _transaction in _transactionList.Where(s => s.ClientId == _clientList.SingleOrDefault(s => s.IsSelect == true).Id))
                {
                    _listMenu.Add(new MenuButton($"--",
                        $"Дата: {_transaction.etl_date.ToString("dd.MM.yy HH:mm")} Отмена: {(_transaction.IsCancelling ? "Да " : "Нет")} Сумма: {_transaction.Value.ToString(_formatSum)}",
                        async () => { }));
                }
                break;
            #endregion

            #region Transactions
            case StepMenu.Transactions:
                _listMenu.Add(new MenuButton($"1", $"Добавить транзакцию (!не работает!)", async () => { }));
                _listMenu.Add(new MenuButton($"--",
                    $"Всего транзакций {_transactionList.Count()} на сумму {_transactionList.Sum(s => s.Value).ToString(_formatSum)}", async () => { }));
                _listMenu.Add(new MenuButton($"--",
                    $"Отменённых транзакций: {_transactionList.Where(s => s.IsCancelling).Count()} на сумму {_transactionList.Where(s => s.IsCancelling).Sum(s => s.Value).ToString(_formatSum)}", async () => { }));
                break;
            #endregion
            default: break;
        }
        _menu = new Menu(_listMenu);
        return;
    }
    /// <summary>
    /// Добавление банка
    /// </summary>
    /// <returns></returns>
    public static async Task CreateBank()
    {
        Console.Clear();
        Log("Создание банка");
        string _town = string.Empty;
        string _name = string.Empty;
        int _raiting = 0;
        while (_town == string.Empty)
        {
            Console.Write("Введите город:");
            _town = Console.ReadLine();
        }
        while (_name == string.Empty)
        {
            Console.Write("Введите наименование филиала:");
            _name = Console.ReadLine();
        }
        while (!(_raiting >= 1 && _raiting <= 5))
        {
            Console.Write("Введите рейтинг (1-5):");
            int.TryParse(Console.ReadLine(), out _raiting);

        }
        bool _result = await _dateBaseService.CreateBank(new Bank() { Town = _town, Name = _name, Raiting = _raiting });
        Log(_result ? "Банк успешно добвален" : "Что то пошло не так :(");
        await LoadData();
        Log("Нажмите любую клавишу для продолжения");
        Console.ReadKey();
        await ChangeMenu(_stackStepMenu.Peek());
    }
    #region Secondary
    /// <summary>
    /// Метод логирования/вывода
    /// </summary>
    public static void Log(string text) => Console.WriteLine($"{DateTime.Now.ToString("HH:mm")}: {text}");
    /// <summary>
    /// Уровень меню
    /// </summary>
    private enum StepMenu
    {
        /// <summary>
        /// Основное меню
        /// </summary>
        Default,
        /// <summary>
        /// Таблицы
        /// </summary>
        Tables,
        /// <summary>
        /// Банки (много)
        /// </summary>
        Banks,
        /// <summary>
        /// Банк (один)
        /// </summary>
        Bank,
        /// <summary>
        /// Клиенты банка (много)
        /// </summary>
        BankByClients,
        /// <summary>
        /// Транзакции банка (много)
        /// </summary>
        BanksByTransactions,
        /// <summary>
        /// Клиенты (много)
        /// </summary>
        Clients,
        /// <summary>
        /// Клиент (один)
        /// </summary>
        Client,
        /// <summary>
        /// Транзакции клиента (много)
        /// </summary>
        ClientByTransactions,
        /// <summary>
        /// Транзакции (много)
        /// </summary>
        Transactions,
        /// <summary>
        /// Транзакция (одна)
        /// </summary>
        Transaction,
        /// <summary>
        /// Добавление банка
        /// </summary>
        CreateBank
    }
    /// <summary>
    /// Меню для управления
    /// </summary>
    private class Menu
    {
        public List<MenuButton> Buttons { get; set; }
        public Menu(List<MenuButton> buttons)
        {
            Buttons = new List<MenuButton>();
            Buttons.Add(new MenuButton("-1", "-------Назад", async () =>
            {
                if (_stackStepMenu.Count() > 1)
                {
                    _stackStepMenu.Pop();
                    await ChangeMenu(_stackStepMenu.Peek());
                }
                else
                    await ChangeMenu(_stackStepMenu.Peek());
            }));
            Buttons.Add(new MenuButton("0", " -------Закрыть приложение", async () => { _exit = true; }));
            Buttons.Add(new MenuButton("-", "-----------------", async () => { }));
            buttons.ForEach(s => { Buttons.Add(s); });
        }
    }
    /// <summary>
    /// Кнопка управления
    /// </summary>
    private class MenuButton
    {
        public string Button { get; private set; }
        public string Info { get; private set; }
        public Func<Task> Execute { get; private set; }
        public MenuButton(string button, string info, Func<Task> execute)
        {
            Button = button;
            Info = info;
            Execute = execute;
        }
    }
    #endregion
}