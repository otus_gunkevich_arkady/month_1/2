﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Month_One_2.Model
{
    /// <summary>
    /// Основной класс для таблиц (для наследования)!
    /// </summary>
    public class Table
    {
        /// <summary>
        /// Ключ. Id
        /// </summary>
        [Key]
        public Guid Id { get; set; }
        /// <summary>
        /// Дата создания записи
        /// </summary>
        public DateTime etl_date { get; set; }
        protected Table()
        {
            Id = Guid.NewGuid();
            etl_date = DateTime.Now;
        }
        /// <summary>
        /// Идентификатор для меню (номер строки)
        /// </summary>
        [NotMapped]
        public int? RowMenu { get; set; }
        /// <summary>
        /// Идентификатор выбора для меню
        /// </summary>
        [NotMapped]
        public bool? IsSelect { get; set; }
    }
}
