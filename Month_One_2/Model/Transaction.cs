﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Month_One_2.Model
{
    /// <summary>
    /// Транзакции совершённые клиентом
    /// </summary>
    public class Transaction : Table
    {
        /// <summary>
        /// ID Клиента
        /// </summary>
        public Guid ClientId { get; set; }
        /// <summary>
        /// Клиент
        /// </summary>
        public Client Client { get; set; }
        /// <summary>
        /// Сумма транзакции
        /// </summary>
        public double Value { get; set; }
        /// <summary>
        /// true = отмена транзакции; 
        /// false = транзакция пройдена;
        /// </summary>
        public bool IsCancelling { get; set; }
    }
}
