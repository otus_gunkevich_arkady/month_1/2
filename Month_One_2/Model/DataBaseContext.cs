﻿using Microsoft.EntityFrameworkCore;

namespace Month_One_2.Model
{
    public class DataBaseContext : DbContext
    {
        public DbSet<Bank> Banks { get; set; }
        public DbSet<Client> Clinets { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DataBaseContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            //Строка подключения
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=Month_One_2;Username=otus_user;Password=qwerty123");
        }
        //Создаём вторичные ключи
        //На всю с EF
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>()
                .HasOne(s => s.Bank)
                .WithMany(s => s.Clients)
                .HasForeignKey(s => s.BankId)
                .HasPrincipalKey(s => s.Id);

            modelBuilder.Entity<Transaction>()
                .HasOne(s => s.Client)
                .WithMany(s => s.Transactions)
                .HasForeignKey(s => s.ClientId)
                .HasPrincipalKey(s => s.Id);
        }
    }
}
