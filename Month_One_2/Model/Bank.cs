﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Month_One_2.Model
{
    /// <summary>
    /// Банк регистрации/обслуживания клиента
    /// </summary>
    public class Bank : Table
    {
        /// <summary>
        /// Город в котором находится банк
        /// </summary>
        public string Town { get; set; }
        /// <summary>
        /// Наименование отделения банка
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Рейтинг банка
        /// </summary>
        public int? Raiting { get; set; }
        /// <summary>
        /// Список клиентов банка
        /// </summary>
        public List<Client>? Clients { get; set; }
        /// <summary>
        /// Количество клиентов
        /// </summary>
        [NotMapped] 
        public int ClientsCount => (Clients is null ? 0 : Clients.Count);
        /// <summary>
        /// Количество транзакций
        /// </summary>
        [NotMapped]
        public int TransactionCount => (Clients is null ? 0 : Clients.Select(s => s.TransactionCount).Sum());
        /// <summary>
        /// Сумма всех транзакций
        /// </summary>
        [NotMapped]
        public double TransactionSum => (Clients is null ? 0 : Clients.Select(s => s.TransactionSum).Sum());
    }
}
