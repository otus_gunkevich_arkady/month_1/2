﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Month_One_2.Model
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class Client : Table
    {
        /// <summary>
        /// Фамилия
        /// </summary>
        public string SurName { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Отчество
        /// </summary>
        public string? LastName { get; set; }
        /// <summary>
        /// Пол
        /// </summary>
        public char Sex { get; set; }
        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime DateOfBirth { get; set; }
        /// <summary>
        /// ID Банка к которому принадлежит клиент
        /// </summary>
        public Guid BankId { get; set; }
        /// <summary>
        /// Банк к которому принадлежит клиент
        /// </summary>
        public Bank Bank { get; set; }
        /// <summary>
        /// Список транзакций клиента
        /// </summary>
        public List<Transaction> Transactions { get; set; }

        /// <summary>
        /// Получение возраста клиента от текущей даты
        /// </summary>
        [NotMapped]
        public double Age => ((DateTime.Now - DateOfBirth).TotalDays / 365);
        /// <summary>
        /// Получение количества транзакций
        /// </summary>
        [NotMapped]
        public int TransactionCount => Transactions.Count();
        /// <summary>
        /// Суммма транзакций
        /// </summary>
        [NotMapped]
        public double TransactionSum => Transactions.Sum(s => s.Value);
    }
}
